const instancesURL = "./instances.json";

let instanceCounts = {
  total: 0,
  tested: 0,
  failed: 0
}

const available_locales = ["es","en","fr","de","ca","it"];

var current_locale = navigator.languages.map(function(language) {
    return language.split('-')[0];
  })
  .find(function(locale) {
    return available_locales.includes(locale);
  });

if (current_locale == undefined) current_locale = available_locales[0];

console.log("Locale:",current_locale);

$(document).ready(function() {
    $("#notice-progress,#notice-finish,#notice-loaded").hide();
    $('#instancesTable').DataTable( {
        ajax: instancesURL,
        stateSave: true,
        stateDuration: 60 * 60 * 24 * 2, //2 days
        info: false,
        order: 2,
        scrollX: true,
        fixedHeader: true,
        paging:   false,
        pageLength: 300,
        columns: [
            { data: "domain", render: function(value, a, data) { return "<a href='https://"+value+"' class='openMeetingDialog'>"+value+"</a>"+ (data.authEnabled ? " &#128274;" : ""); } },
            { data: "download", type: "num", defaultContent: "...", className: "dt-body-right", render: $.fn.dataTable.render.jitsimeterNumber("kbps")},
            { data: "rtt", type: "num-fmt", defaultContent: "...", className: "dt-body-right", render:  $.fn.dataTable.render.jitsimeterNumber("ms") },
            { data: "corporateStun", className: "dt-body-center", render: function(value) { return (value === true) ? "&#10006;" : "&#10004;"; } },
            { data: "corporateHoster", className: "dt-body-center", render: function(value) { return (value === true) ? "&#10006;" : "&#10004;"; } },
            { data: "analyticsEnabled", className: "dt-body-center", render: function(value) { return (value === true) ? "&#10006;" : "&#10004;"; } },
            { data: "countryCode", className: "dt-body-center" },
            { data: "hoster", defaultContent: "-" },
            { data: "jitsiVersion", defaultContent: "-" }
        ],
        initComplete: function() {
          var info = this.DataTable().page.info();
          instanceCounts.total = info.recordsTotal;
          $('#instanceCountsTotal').html(info.recordsTotal);
          restoreState(this.DataTable());
          updateProgress();
        },
        stateSaveParams: function (settings, data) {
          if (window.jitsimeterState && data.jitsimeterState != window.jitsimeterState) {
          console.log("Saving state");
          data.jitsimeterState = window.jitsimeterState;
        }
        },
        stateLoaded: function (settings, data) {
          if (data.jitsimeterState) {
            console.log("Loaded state");

            $("#notice-loaded").show();
            window.jitsimeterState=data.jitsimeterState;

          }
        }
    } );

    $("#start").click(startTest)
    $("#helpButton").click(function() {
      $("#helpText").slideToggle();
    })
    $("#shareButton").click(shareResults);
    $("#downloadButton").click(downloadResults);

     //Multilang
     available_locales.forEach(function(locale) {
       if (locale == current_locale) return;
       document.querySelectorAll('html [lang='+locale+']').forEach(function(el) {
         el.parentNode.removeChild(el);
       });
     });

} );

function shareResults() {
  if (navigator.share) {
    navigator.share({
      title: 'Jitsimeter',
      text: 'Jitsimeter te ayuda a decidir qué instancia de Jitsi te conviene utilizar para mejorar tus videollamadas.',
      url: window.location.href,
    })
      .then(() => console.log('Successful share'))
      .catch((error) => console.log('Error sharing', error));
  }
}

function downloadResults() {
  const rows = [
      ["date","domain", "jitsimeter_download", "jitsimeter_rtt", "source"]
  ];

  let storedState = JSON.parse(window.jitsimeterState);

  for (r in storedState.resultsArray) {
    let rState = storedState.resultsArray[r];
    let rStateDate = storedState.datesArray[r];

    for (i in rState[0]) {
      rows.push([rStateDate,rState[0][i],rState[1][i],rState[2][i],window.location.href]);
    }
  }

  let csvContent = "data:text/csv;charset=utf-8,"
      + rows.map(e => e.join(",")).join("\n");

  var encodedUri = encodeURI(csvContent);
  var link = document.createElement("a");
  link.setAttribute("href", encodedUri);
  link.setAttribute("download", "jitsimeter_results.csv");
  document.body.appendChild(link); // Required for FF

  link.click();
}

//
// $.on("click",".openMeetingDialog", (e) => {
//   domain = $(e.currentTarget).attr("href");
//   $("#meetingDialog").show();
//   $("#meetingDialog").data("domain",domain);
//   e.preventDefault();
//   e.stopPropagation();
//   return false;
// })
//
// $.on("click","#startMeeting", (e) => {
//   $("#meetingDialog").data("domain");
//
// })

$.fn.dataTable.render.jitsimeterNumber = function ( unit ) {
  return function ( value, type, row ) {
    if (typeof value == "undefined") return;
    switch (value) {
      case false:
        if (type === "display" || type === "filter") {
          return "<span title='No se pudo medir. Algunos servidores rechazan solicitudes CORS.'>&#10006;</span>"; // cross
        } else {
          return 0;
        }
        break;
      case "progress":
        if (type === "display" || type === "filter") {
          return "<span title='En evaluación'>⟳</span>"; // reload
        } else {
          return 0;
        }
        break;
      default:
      if (type === "display" || type === "filter") {
        if (value) {
          return Number(value).toLocaleString() + " " + unit;
        }
      } else {
        return value;
      }
    }
  }
}


async function startTest() {
  let table = $('#instancesTable').DataTable();
  let data = table.data();
  $("#notice-stop").hide();
  $("#notice-loaded").hide();
  $("#notice-progress").show()

  $.each(data, function(i,instance) {
    try {
      instance.table = table;
      $cell = $("table td:contains('"+instance.domain+"')");
      let index = instance.table.cell($cell).index();

      instance.row = instance.table.row(index.row);
      instance.downloadCell = instance.table.cell({row: index.row, column: 1});
      instance.rttCell = instance.table.cell({row: index.row, column: 2});

      if (instance.available == false) {
        failed(instance);
        return true;
      }

      setTimeout(function() {
          jitsi_test($('#instancesTable'),i, instance);
      }, 1000*i)
    }
    catch(e) {
      console.error("Error jitsi_test:",e,i,instance);
    }
  })
}

function updateProgress() {

  $("#progress").attr("max",instanceCounts.total);
  $("#progress").attr("value",(instanceCounts.failed+instanceCounts.tested));
  $('#instanceCountsFailed').html(instanceCounts.failed);


  if (instanceCounts.total == (instanceCounts.failed+instanceCounts.tested)) {
    endTest()
  }
  $('#instancesTable').DataTable().order(1).draw();
}

function endTest() {
  $("#notice-progress").hide();
  $("#notice-finish").show()

  saveState();
}

function saveState() {
  console.log("Creating state");
  let currentResults = $("#instancesTable").DataTable().columns([0,1,2]).data().toArray();
  let currentResultsDate = new Date();

  let storedState = window.jitsimeterState ? JSON.parse(window.jitsimeterState) : null;

  if (storedState) {
    currentState = storedState
  }
  else {
    currentState = {
      resultsArray: [],
      datesArray: []
    };

  }

  currentState.resultsArray.push(currentResults);
  currentState.datesArray.push(currentResultsDate);

  window.jitsimeterState = JSON.stringify(currentState);
}

function restoreState(table) {
  if (!window.jitsimeterState) return false;

  let storedState = JSON.parse(window.jitsimeterState);

  let lastState = storedState.resultsArray[(storedState.resultsArray.length - 1)];
  let lastStateDate = storedState.datesArray[(storedState.resultsArray.length - 1)];

  $("#savedStateDate").text(new Date(lastStateDate).toLocaleString());

  for (i in lastState[0]) {
    $cell = $("table td:contains('"+lastState[0][i]+"')");
    let index = table.cell($cell).index();
    if (index) {
      let row = table.row(index.row);
      let downloadCell = table.cell({row: index.row, column: 1});
      let rttCell = table.cell({row: index.row, column: 2});

      downloadCell.data(lastState[1][i]);
      rttCell.data(lastState[2][i]);

      //TODO: Mark the row as stale
    }
    else {
      console.error(lastState[0][i],$cell);
    }
  }

}


function jitsi_test(table,rowNumber, instance) {
  instance.downloadCell.data("progress");
  instance.rttCell.data("progress");
  updateProgress();

  let testURL = "https://"+instance.domain+"/sounds/outgoingStart.wav";

  TestResponse(testURL, function(response) {
    // console.log("jitsi_rtt_test success", response)
    if (response != -1) {
      instance.rttCell.data(response);

      TestDownload(testURL, function(kbps) {
        // console.log("jitsi_download_test success", kbps)
        instance.downloadCell.data(kbps);
        instanceCounts.tested+=1;
        updateProgress();
      } )
    }
    else {
      failed(instance);
    }
  } )


}

function failed(instance) {
  instanceCounts.failed+=1;
  instance.rttCell.data(false);
  instance.downloadCell.data(false);
  updateProgress();
}

//** Test the download speed
// based in https://github.com/skycube/jqspeedtest
function TestDownload(url,callback){
  var sendDate = (new Date()).getTime();
  // console.log("TestDownload", url);
  $.ajax({
    type: "GET",
    url: url,
    timeout: 60000,
    cache: false,
    success: function(){
      var receiveDate = (new Date()).getTime();
      var duration = (receiveDate - sendDate) / 1000;
      var bitsLoaded = 206.34 * 1024 * 8;
      var speedBps = (bitsLoaded / duration).toFixed(2);
      var speedKbps = (speedBps / 1024).toFixed(0);
      var speedMbps = (speedKbps / 1024).toFixed(2);
      var speedGbps = (speedMbps / 1024).toFixed(2);

      callback(speedKbps);

    },
    error: function() { callback( -1) }
  });
}


//** Test Response time
// based in https://github.com/skycube/jqspeedtest
function TestResponse(url,callback){
  var sendDate = (new Date()).getTime();
  // console.log("TestResponse",sendDate)
  $.ajax({
    type: "HEAD",
    url: url,
    timeout: 5000,
    cache: false,
    success: function(a,b,c){
      // console.log("tr suc",a,b,c)
      var receiveDate = (new Date()).getTime();
      var response = receiveDate - sendDate;

      callback(response);
    },
    error: function() { callback( -1) }
  });
}
